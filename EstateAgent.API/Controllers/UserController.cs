using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using EstateAgent.API.Data;
using EstateAgent.API.Dtos;
using EstateAgent.API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EstateAgent.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IHousingRepository _repo;
        private readonly IMapper _mapper;
        private readonly IAuthRepository _authRepo;
        public UserController(IHousingRepository repo, IMapper mapper, IAuthRepository authRepo)
        {
            this._authRepo = authRepo;
            this._mapper = mapper;
            this._repo = repo;
        }

        [HttpGet("users")]
        public async Task<IActionResult> GetUsers()
        {
            var users = await _repo.GetUsers(); // Uses the repository to get the users from the database
            var usersToReturn = _mapper.Map<IEnumerable<UsersForList>>(users); // Maps the houses' details to a differnt object with less detail
            return Ok(usersToReturn); // Returns the users to the user from the API
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetUser(int userId)
        {
            var user = await _repo.GetUser(userId); // Uses the repository to get the user from the database
            Console.WriteLine(user);
            var userToReturn = _mapper.Map<UserForEditDto>(user); // Maps the user to a suitable model
            return Ok(userToReturn); // Returns the user to the user from the API
        }

        [HttpPost("update")]
        public async Task<IActionResult> UpdateUser(UserForEditDto userForEdit)
        {
            if(userForEdit.Password.Length < 6 && userForEdit.Password.Length > 0)
            {
                return NotFound();
            }

            User user = await _repo.GetUser(userForEdit.Id);
            user.Username = userForEdit.Username;
            user.Email = userForEdit.Email;

            if(userForEdit.Password.Length != 0){
                byte[] passwordHash, passwordSalt; 
                _authRepo.CreatePasswordHash(userForEdit.Password, out passwordHash, out passwordSalt);
                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
            }

            await _repo.SaveAll();

            return Ok(true);
        }
    }
}