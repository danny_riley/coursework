using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EstateAgent.API.Data;
using EstateAgent.API.Dtos;
using EstateAgent.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EstateAgent.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private readonly IMessageRepository _repo;
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        public MessageController(IMessageRepository repo, DataContext context, IMapper mapper)
        {
            this._context = context;
            this._repo = repo;
            this._mapper = mapper;
        }

        [HttpPost("send")]
        public IActionResult sendMessage(MessageDto message)
        {
            User sender = _context.Users.FirstOrDefault(x => x.Id == message.senderId);
            User recipient = _context.Users.FirstOrDefault(x => x.Id == message.recipientId);
            _repo.SendMessage(sender, recipient, message.content);
            return Ok(true);
        }

        [HttpPost("users")]
        public async Task<IActionResult> getUsers(UserIdDto userId)
        {
            User user = _context.Users.FirstOrDefault(x => x.Id == userId.UserId); // Gets the user from the database
            IEnumerable<User> users = await _repo.GetUsers(user); // Gets the users the user has messaged
            IEnumerable<UserForMessageList> usersToReturn = _mapper.Map<IEnumerable<UserForMessageList>>(users);
            foreach(UserForMessageList userToReturn in usersToReturn){
                IEnumerable<Message> messages = await _repo.GetMessages(user, _context.Users.FirstOrDefault(x => x.Id == userToReturn.Id));
                Message message = messages.LastOrDefault();
                if(message != null){
                    userToReturn.LatestMessage = message.Content;
                    userToReturn.LatestMessageSender = message.Sender.Username;
                    userToReturn.Sent = message.DateSent;
                }
            }
            return Ok(usersToReturn); // Maps users to less detailed version
        }

        [HttpPost("messages")]
        public async Task<IActionResult> getMessages(UserToUserDto userIds)
        {
            User sender = _context.Users.FirstOrDefault(x => x.Id == userIds.SenderId); // Gets users from the database
            User recipient = _context.Users.FirstOrDefault(x => x.Id == userIds.RecipientId);

            IEnumerable<Message> messages = await _repo.GetMessages(sender, recipient); // Gets messages from these users
            IEnumerable<MessageForDisplay> messagesToReturn = _mapper.Map<IEnumerable<MessageForDisplay>>(messages);
            return Ok(messagesToReturn); // Maps user details to just name and picture
        }
    }
}