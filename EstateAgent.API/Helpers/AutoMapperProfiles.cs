using System.Linq;
using AutoMapper;
using EstateAgent.API.Dtos;
using EstateAgent.API.Models;

namespace EstateAgent.API.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<House, HouseForListDto>()
                .ForMember(dest => dest.PhotoUrl, opt => {
                    opt.MapFrom(src => src.Photos.FirstOrDefault(p => p.Id > 0).Url);
                });
            CreateMap<House, HouseForDetailedDto>();
            CreateMap<Photo, PhotosForDetailedDto>();
            CreateMap<User, UsersForList>()
                .ForMember(dest => dest.LastActive, opt => {
                    opt.MapFrom(src => src.LastActive.Year.ToString() + '/' + src.LastActive.Month.ToString() + '/' + src.LastActive.Day.ToString());
                });
            CreateMap<User, UserForEditDto>()
                .ForMember(dest => dest.Password, opt => {
                    opt.MapFrom(dest => "");
                });
            CreateMap<Message, MessageForDisplay>();
            CreateMap<User, UserForMessageList>()
                .ForMember(dest => dest.LatestMessage, opt => opt.Ignore())
                .ForMember(dest => dest.LatestMessageSender, opt => opt.Ignore())
                .ForMember(dest => dest.Sent, opt => opt.Ignore());
            CreateMap<Photo, PhotoForReturnDto>();
            CreateMap<PhotoForCreationDto, Photo>();
        }
    }
}