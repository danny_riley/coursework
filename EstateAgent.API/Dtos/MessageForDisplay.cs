namespace EstateAgent.API.Dtos
{
    public class MessageForDisplay
    {
        public string Content { get; set; }
        public UserForReturn Sender { get; set; }
        public UserForReturn Recipient { get; set; }
    }
}