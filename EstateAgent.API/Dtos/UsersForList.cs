using System;

namespace EstateAgent.API.Dtos
{
    public class UsersForList
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string LastActive { get; set; }
        public string PhotoUrl { get; set; }
    }
}