using EstateAgent.API.Models;

namespace EstateAgent.API.Dtos
{
    public class HouseForListDto
    {
        public int Id { get; set; }
        public int PAON { get; set; }
        public string Street { get; set; }
        public string Postcode { get; set; }
        public int Price { get; set; }
        public int Bedrooms { get; set; }
        public string PhotoUrl { get; set; }
    }
}