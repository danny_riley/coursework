namespace EstateAgent.API.Dtos
{
    public class MessageDto
    {
        public int senderId;
        public int recipientId;
        public string content;
    }
}