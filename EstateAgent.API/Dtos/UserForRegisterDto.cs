using System.ComponentModel.DataAnnotations;

namespace EstateAgent.API.Dtos
{
    public class UserForRegisterDto
    {
        [Required]
        public string Username { get; set; }
        
        [Required] // Sets up validation for the password field
        [StringLength(50, MinimumLength=6, ErrorMessage = "Your password must be at least 6 characters long")]
        public string Password { get; set; }
    }
}