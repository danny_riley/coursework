using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EstateAgent.API.Dtos
{
    public class UserForEditDto
    {
        public int Id { get; set; }

        [Required]
        public string Username { get; set; }
        
        [Required]
        public string Email { get; set; }

        public string Password { get; set; }
    }
}