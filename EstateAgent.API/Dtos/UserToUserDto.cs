namespace EstateAgent.API.Dtos
{
    public class UserToUserDto
    {
        public int SenderId { get; set; }
        public int RecipientId { get; set; }
    }
}