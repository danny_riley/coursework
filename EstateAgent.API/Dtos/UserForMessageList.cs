using System;

namespace EstateAgent.API.Dtos
{
    public class UserForMessageList
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string LastActive { get; set; }
        public string PhotoUrl { get; set; }
        public string LatestMessage { get; set; }
        public string LatestMessageSender { get; set; }
        public DateTime Sent { get; set; }
    }
}