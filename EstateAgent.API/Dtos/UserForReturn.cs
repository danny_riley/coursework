namespace EstateAgent.API.Dtos
{
    public class UserForReturn
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string PhotoUrl { get; set; }
    }
}