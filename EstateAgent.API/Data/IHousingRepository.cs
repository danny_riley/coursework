using System.Collections.Generic;
using System.Threading.Tasks;
using EstateAgent.API.Models;

namespace EstateAgent.API.Data
{
    public interface IHousingRepository
    {
        void Add<T>(T entity) where T: class;
        void Delete<T>(T entity) where T: class;
        Task<bool> SaveAll();
        Task<IEnumerable<User>> GetUsers();
        Task<User> GetUser(int id);
        Task<IEnumerable<House>> GetHouses();
        Task<IEnumerable<House>> GetUserHouses(User user);
        Task<House> GetHouse(int id);
        Task<bool> AddBookmark(int houseId, int userId);
        Task<IEnumerable<Bookmark>> GetBookmarks(int userId);
        Task<Photo> GetPhoto(int id);
    }
}