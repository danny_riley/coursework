using System.Collections.Generic;

using System.Threading.Tasks;
using EstateAgent.API.Models;
using Microsoft.EntityFrameworkCore;

namespace EstateAgent.API.Data
{
    public class HousingRepository : IHousingRepository
    {
        private readonly DataContext _context;

        public HousingRepository(DataContext context)
        {
            this._context = context; // Allows the repository to access the database
        }

        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity); // Adds an entity to the database
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity); // Removes an entity from the database
        }

        public async Task<House> GetHouse(int id)
        {
            var house = await _context.Houses.Include(p => p.Photos).FirstOrDefaultAsync(h => h.Id == id);
            return house; // Gets a house by the Id of the hosue
        }

        public async Task<IEnumerable<House>> GetHouses()
        {
            var houses = await _context.Houses.Include(p => p.Photos).ToListAsync();
            return houses; // Gets all of the houses in the database
        }

        public async Task<IEnumerable<Bookmark>> GetBookmarks(int userId)
        {
            var user = await _context.Users.Include(x => x.Bookmarks).ThenInclude(x => x.House).ThenInclude(x => x.Photos).FirstOrDefaultAsync(x => x.Id == userId);
            var bookmarks = user.Bookmarks;

            return bookmarks; // Gets all of the houses a user has bookmarked
        }

        public async Task<User> GetUser(int id)
        {
            var user = await _context.Users.Include(p => p.Houses).FirstOrDefaultAsync(u => u.Id == id);
            return user; // Gets a user by the Id of the user
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            var users = await _context.Users.Include(p => p.Houses).ToListAsync();
            return users; // Gets all of the users in the database
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0; // Saves all changes which have been made to the database
        }

        public async Task<IEnumerable<House>> GetUserHouses(User user)
        {
            var houseUser = await _context.Users.Include(p => p.Houses).ThenInclude(h => h.Photos)
                .FirstOrDefaultAsync(p => p == user); // Gets all of the houses from a specific user
            var houses = houseUser.Houses;
            return houses;
        }

        public async Task<bool> AddBookmark(int houseId, int userId)
        {
            var user = await _context.Users.Include(x => x.Bookmarks).ThenInclude(x => x.House).FirstOrDefaultAsync(x => x.Id == userId); // Gets the user and their bookmarks from the database
            var userBookmarks = user.Bookmarks; // Gets a list of the users bookmarks
            if(userBookmarks != null){
                foreach(Bookmark bm in userBookmarks) // Checks to see if the bookmark is in the database
                {
                    if(bm.House.Id == houseId)
                    {
                        _context.Remove(bm); // Removes it if it is in the database
                        _context.SaveChanges();
                        return true;
                    }
                }
            }

            var house = await _context.Houses.FirstOrDefaultAsync(x => x.Id == houseId); // Gets the house from the database
            Bookmark bookmark = new Bookmark(); // Creates a new empty bookmark
            bookmark.User = user; // Adds the details to the bookmark
            bookmark.House = house;
            user.Bookmarks.Add(bookmark); // Adds the bookmark to the user's bookmarks
            _context.SaveChanges(); // Saves changes to the database
            return true; // Returns true to the controller
        }

        public async Task<Photo> GetPhoto(int id)
        {
            var photo = await _context.Photos.FirstOrDefaultAsync(p => p.Id == id);
            return photo;
        }
    }
}