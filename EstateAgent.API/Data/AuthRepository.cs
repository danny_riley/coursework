using System;
using System.Threading.Tasks;
using EstateAgent.API.Models;
using Microsoft.EntityFrameworkCore;

namespace EstateAgent.API.Data
{
    public class AuthRepository : IAuthRepository
    {
        private readonly DataContext _context;
        public AuthRepository(DataContext context)
        {
            this._context = context;

        }
        public async Task<User> Login(string username, string password)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Username == username);

            if (user == null) // If there is no user in the database with the specified username
                return null;  // dont allow the user to log in

            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null; // If the hash of the password entered does not match the hash
                             // of the password in the database dont allow the user to log in

            return user; // If both of these tests pass allow the user to log in
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using(var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt)) // Uses the password hash as the key
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for(int i = 0; i < computedHash.Length; i++)
                {
                    if(computedHash[i] != passwordHash[i]) // Compares each element of the computed
                        return false;                      // has to the actual hash
                }
                return true; // If the computed hash is equal to the actual hash return true
            } 
        }

        public async Task<User> Register(User user, string password)
        {
            byte[] passwordHash, passwordSalt; //Creates the new variables to store the has and salt
            CreatePasswordHash(password, out passwordHash, out passwordSalt);
            user.PasswordHash = passwordHash; // Sets the hash and salt of the user's password to the
            user.PasswordSalt = passwordSalt; // hash and salt calculated in the above method

            await _context.Users.AddAsync(user); // Adds the user to the database
            await _context.SaveChangesAsync(); // Saves the changes to the database

            return user;
        }

        public void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using(var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key; // Sets the password salt to the secret hmac key
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                // Computes the hash of the password, this will use the salt set above
            } 
        }

        public async Task<bool> UserExists(string username)
        {
            if(await _context.Users.AnyAsync(x => x.Username == username))
                return true; // Querys the database to see if a user exists with the username specified

            return false;
        }
    }
}