using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EstateAgent.API.Models;

namespace EstateAgent.API.Data
{
    public class MessageRepository : IMessageRepository
    {
        private readonly DataContext _context;

        public MessageRepository (DataContext context)
	    {
            _context = context;
	    }

        public async Task<IEnumerable<Message>> GetMessages(User sender, User recipient)
        {
            IEnumerable<Message> messages = _context.Messages.Where(x => (x.Sender == sender && x.Recipient == recipient) | (x.Sender == recipient && x.Recipient == sender)).AsEnumerable();
            return(messages);
        }

        public bool SendMessage(User sender, User recipient, string content)
        {
            Message message = new Message // Creates the message
                {
                    Sender = sender,
                    Recipient = recipient,
                    DateSent = DateTime.Now,
                    Content = content
                };

            _context.Messages.AddAsync(message); // Adds messaage to the database
            _context.SaveChanges();
            
            return(true);
        }
        
        public async Task<IEnumerable<User>> GetUsers(User user)
        {
            IEnumerable<User> users = _context.Messages.Where(x => x.Sender == user | x.Recipient == user).Select(x => x.Sender).Distinct().AsEnumerable();
            IEnumerable<User> usersMore = _context.Messages.Where(x => x.Sender == user | x.Recipient == user).Select(x => x.Recipient).Distinct().AsEnumerable();
            users = users.Concat(usersMore);
            users = users.Distinct();
            users = users.Where(x => x != user);
            return(users);
        }
    }
}