using System.Collections.Generic;
using System.Threading.Tasks;
using EstateAgent.API.Models;

namespace EstateAgent.API.Data
{
    public interface IMessageRepository
    {
        Task<IEnumerable<Message>> GetMessages(User sender, User recipient);
        bool SendMessage(User sender, User recipient, string content);
        Task<IEnumerable<User>> GetUsers(User user);
    }
}