using EstateAgent.API.Models;
using Microsoft.EntityFrameworkCore;

namespace EstateAgent.API.Data
{
    public class DataContext : DbContext
    {
        public DataContext (DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<House> Houses {get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Bookmark> Bookmarks { get; set; }
        public DbSet<Message> Messages { get; set; }
    }
}