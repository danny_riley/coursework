using System.Collections.Generic;
using EstateAgent.API.Models;
using Newtonsoft.Json;

namespace EstateAgent.API.Data
{
    public class Seed
    {
        private readonly DataContext _context;
        public Seed(DataContext context)
        {
            this._context = context;
        }

        public void SeedUsers() 
        {
            var userData = System.IO.File.ReadAllText("Data/UserSeedData.json"); // Reads the data from the JSON file
            var users = JsonConvert.DeserializeObject<List<User>>(userData); // Creats an object from the JSON data
            foreach (var user in users) // Loops through all of the users in the object
            {
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash("password", out passwordHash, out passwordSalt); // Hashes the username and password

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
                user.Username = user.Username.ToLower();
                

                _context.Users.Add(user); // Adds the user to the database
            }

            _context.SaveChanges(); // Saves the changes to the database
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using(var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key; // Sets the password salt to the secret hmac key
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                // Computes the hash of the password, this will use the salt set above
            } 
        }
    }
}