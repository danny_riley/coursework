using System;
using System.Collections.Generic;

namespace EstateAgent.API.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastActive { get; set; }
        public string PhotoUrl { get; set; }
        public ICollection<House> Houses { get; set; }
        public ICollection<Bookmark> Bookmarks { get; set; }
        public string VerificationCode { get; set; }
        public bool Verified { get; set; }
    }
}


