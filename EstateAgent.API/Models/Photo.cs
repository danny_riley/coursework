using System;

namespace EstateAgent.API.Models
{
    public class Photo
    {
        public int Id { get; set; }
        public string PublicId { get; set; }
        public string Url { get; set; }
        public DateTime DateAdded { get; set; }
        public House House { get; set; }
        public int HouseId { get; set; }
    }
}