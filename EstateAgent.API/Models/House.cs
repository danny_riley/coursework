using System.Collections.Generic;

namespace EstateAgent.API.Models
{
    public class House
    {
        public int Id { get; set; }
        public int PAON { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public int Price { get; set; }
        public int Bedrooms { get; set; }
        public string Description { get; set; }
        public ICollection<Photo> Photos { get; set; }
    }
}