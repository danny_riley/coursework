﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EstateAgent.API.Migrations
{
    public partial class AddedDescriptionToHouse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Houses",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Houses");
        }
    }
}
