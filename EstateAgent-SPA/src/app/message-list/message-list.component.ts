import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { HttpClient } from '@angular/common/http';
import { AlertifyService } from '../_services/alertify.service';

@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.css']
})
export class MessageListComponent implements OnInit {

  users: any;

  data: any;

  constructor(private router: Router, public authService: AuthService,
    private http: HttpClient, private alertify: AlertifyService) { }

  ngOnInit() {
    this.data = {
      'UserId': this.authService.decodedToken.nameid
    };
    this.getUsers();
  }

  getUsers() {
    this.http.post('http://localhost:5000/api/message/users', this.data).subscribe(response => {
        this.users = response;
        console.log(this.users);
      }
    );
  }

}
