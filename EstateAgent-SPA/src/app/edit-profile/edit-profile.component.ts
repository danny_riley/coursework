import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { HttpClient } from '@angular/common/http';
import { AlertifyService } from '../_services/alertify.service';
import { Router } from '@angular/router';
import { logging } from 'protractor';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

  user: any;

  constructor(private router: Router, public authService: AuthService,
    private http: HttpClient, private alertify: AlertifyService) { }

  ngOnInit() {
    this.getUserDetails(); // Calls the get users details method
  }

  getUserDetails() {
    this.http.get('http://localhost:5000/api/user/' + this.authService.decodedToken.nameid)
    .subscribe(response => {
      this.user = response; // Gets the users details so that they can be bound to the properties
    }, error => {
      this.alertify.error(error);
    });
  }

  saveChanges() {
    this.http.post('http://localhost:5000/api/user/update', this.user).subscribe(response => {
      this.alertify.success('You have successfully updated your details');
      this.login();
      this.router.navigate(['/houses']);
    },
    error => {
      this.alertify.error(error);
    });
  }

  login() {
    this.authService.login(this.user).subscribe(next => {
    },
    error => {
      this.alertify.error(error); // Sends error message to the user
    }, () => {
      this.router.navigate(['/houses']); // Navigates to the houses list when the user logs in
    });
  }

}
