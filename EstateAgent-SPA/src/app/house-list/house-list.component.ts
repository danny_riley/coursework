import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-house-list',
  templateUrl: './house-list.component.html',
  styleUrls: ['./house-list.component.css']
})
export class HouseListComponent implements OnInit {

  data: any = {};
  houses: any;

  constructor(private http: HttpClient, public authService: AuthService) { }

  ngOnInit() {
    this.getHouses();
  }

  getHouses() {
    this.http.get('http://localhost:5000/api/housing').subscribe(response => {
      this.houses = response;
    }, error => {
      console.log(error);
    });
  }

  addBookmark(houseId: string) {
    this.data = {
      'userid': this.authService.decodedToken.nameid,
      'houseid': houseId
    };

    this.http.post('http://localhost:5000/api/housing/bookmark', this.data).subscribe();
  }

}
