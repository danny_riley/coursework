import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router,
    private alertify: AlertifyService) {}

  canActivate(): boolean {
    if (this.authService.loggedIn()) {
      return true; // If the user is logged in they can access a route
    }

    this.alertify.error('You need to log in before you can access this part of the site');
    this.router.navigate(['home']); // Navigates the user to home if they cannot access a route
    return false;
  }
}
