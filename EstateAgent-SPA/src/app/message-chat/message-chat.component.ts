import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { HttpClient } from '@angular/common/http';
import { AlertifyService } from '../_services/alertify.service';

@Component({
  selector: 'app-message-chat',
  templateUrl: './message-chat.component.html',
  styleUrls: ['./message-chat.component.css']
})
export class MessageChatComponent implements OnInit {

  data: any;
  messages: any;
  message: any;
  messageContent: any;
  senderId = this.authService.decodedToken.nameid;
  recipientId = this.route.snapshot.params['id'];
  sender: any;


  constructor(private router: Router, public authService: AuthService,
    private http: HttpClient, private alertify: AlertifyService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.data = {
      'SenderId': this.authService.decodedToken.nameid,
      'RecipientId': this.route.snapshot.params['id']
    };
    this.getMessages();
    this.getSender();
  }

  getMessages() {
    this.http.post('http://localhost:5000/api/message/messages', this.data).subscribe(response => {
      this.messages = response;
      this.getMessages();
    });
  }

  getSender() {
    this.http.get('http://localhost:5000/api/user/' + this.route.snapshot.params['id']).subscribe(response => {
      this.sender = response;
      // console.log(response);
    }, error => {
      console.log(error);
    });
  }

  isRight(message) {
    if (message.Sender.Id === this.recipientId) {
      return(true);
    }
  }

  sendMessage() {
    this.message = {
      'senderId': this.senderId,
      'recipientId': this.recipientId,
      'content': this.messageContent
    };
    console.log(this.messageContent);
    this.http.post('http://localhost:5000/api/message/send', this.message).subscribe(response => {
      this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(() =>
      this.router.navigate(['messages/' + this.recipientId]));
    });
  }

}
