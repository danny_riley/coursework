import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-house-detailed',
  templateUrl: './house-detailed.component.html',
  styleUrls: ['./house-detailed.component.css']
})
export class HouseDetailedComponent implements OnInit {

  house: any;
  mapUrl: any;
  loaded = false;

  constructor(private http: HttpClient, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getHouse();
  }

  getHouse() {
    this.http.get('http://localhost:5000/api/housing/' + this.route.snapshot.params['id']).subscribe(response => {
      this.house = response;
      this.mapUrl = 'https://maps.google.com/maps?q=' + this.house['postcode'] + '&amp;t=&amp;z=15&amp;ie=UTF8&amp;iwloc=&amp;output=embed';
      this.loaded = true;
    }, error => {
      console.log(error);
    });
  }

}
