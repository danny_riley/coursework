import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { AuthService } from './_services/auth.service';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { ErrorInterceptorProvider } from './_services/error.interceptor';
import { AlertifyService } from './_services/alertify.service';
import { HouseListComponent } from './house-list/house-list.component';
import { BookmarksComponent } from './bookmarks/bookmarks.component';
import { appRoutes } from './routes';
import { AuthGuard } from './_guards/auth.guard';
import { UserListComponent } from './user-list/user-list.component';
import { HouseDetailedComponent } from './house-detailed/house-detailed.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { MessageListComponent } from './message-list/message-list.component';
import { MessageChatComponent } from './message-chat/message-chat.component';

@NgModule({
   declarations: [
      AppComponent,
      NavComponent,
      HomeComponent,
      RegisterComponent,
      HouseListComponent,
      BookmarksComponent,
      UserListComponent,
      HouseDetailedComponent,
      EditProfileComponent,
      MessageListComponent,
      MessageChatComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      FormsModule,
      BsDropdownModule.forRoot(),
      CarouselModule.forRoot(),
      RouterModule.forRoot(appRoutes)
   ],
   providers: [
      AuthService,
      ErrorInterceptorProvider,
      AlertifyService,
      AuthGuard
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
