import { MessageChatComponent } from './message-chat/message-chat.component';
import { MessageListComponent } from './message-list/message-list.component';
import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HouseListComponent } from './house-list/house-list.component';
import { BookmarksComponent } from './bookmarks/bookmarks.component';
import { AuthGuard } from './_guards/auth.guard';
import { UserListComponent } from './user-list/user-list.component';
import { HouseDetailedComponent } from './house-detailed/house-detailed.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';

export const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {
        path: '', // A path which all guarded routes have to go through
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuard], // Activates the AuthGuard component
        children: // Only allows users who are logged in to access these routes
        [
            {path: 'houses', component: HouseListComponent},
            {path: 'bookmarks', component: BookmarksComponent},
            {path: 'users', component: UserListComponent},
            {path: 'house/:id', component: HouseDetailedComponent},
            {path: 'profile', component: EditProfileComponent},
            {path: 'messages', component: MessageListComponent},
            {path: 'messages/:id', component: MessageChatComponent}
        ]
    },
    {path: '**', redirectTo: '', pathMatch: 'full'} // Goes to the home component if no route is given
    // Or the wrong route is given
];
